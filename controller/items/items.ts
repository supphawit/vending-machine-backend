import { Items } from './../../src/entity/Items';
import { getConnection } from '../../database/db';
import { NextApiRequest, NextApiResponse } from 'next';

export const getAllItems = async (req: NextApiRequest, res: NextApiResponse) => {
    const conn = await getConnection();
    let itemsRepository = conn.getRepository(Items)
    const items = await itemsRepository.find({
        order: {
            id: "ASC"
        }
    });
    conn.close()
    res.json(items)
}
