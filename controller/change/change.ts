import { getConnection } from '../../database/db';
import { NextApiRequest, NextApiResponse } from 'next';
import { Change } from '../../src/entity/Change'

export const getChange = async (req: NextApiRequest, res: NextApiResponse) => {
    const conn = await getConnection();
    let changeRepo = conn.getRepository(Change)
    const change = await changeRepo.find({
        order: {
            id: "ASC"
        }
    });
    conn.close()
    res.json(change)
}
