import "reflect-metadata";
import { Connection, createConnection } from "typeorm";
import { Items } from './../src/entity/Items';
import { Change } from "../src/entity/Change";
import { SaleOrder } from "../src/entity/SaleOrder";

let conn: Connection;

const getConnection = async (): Promise<Connection> => {
    if (conn) {
        conn.close()
    }
    try {
        conn = await createConnection({
            type: "postgres",
            host: "localhost",
            port: 5432,
            username: "root",
            password: "passw0rd",
            database: "postgres",
            entities: [
                Items,
                Change,
                SaleOrder
            ],
            synchronize: true,
            logging: false
        });
        return conn;
    } catch (err: any) {
        // console.error(err)
        throw err
    }
}

export { getConnection };



// docker run -d \
//     	--name postgresql \
//     	-e POSTGRES_USER=root \
// 	-e POSTGRES_PASSWORD=passw0rd \
// 	-p 5432:5432 \
// 	postgres