import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Change {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    amount?: number;

    @Column()
    qty?: number;
}
