import { Entity, PrimaryGeneratedColumn, Column, OneToOne, } from "typeorm";

@Entity()
export class Items {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column('varchar')
    name?: string;

    @Column('int4')
    price?: number;

    @Column('int4')
    qty?: number;

    @Column('int4')
    img_size?: number;

}
