import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class SaleOrder {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    item_name?: string;

    @Column()
    price?: number;

    // @Column()
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    create_at?: Date;

}
