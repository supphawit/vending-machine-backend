import cors from 'cors';
import { NextApiResponse, NextApiRequest } from 'next';
import nc from 'next-connect';
import { getChange } from '../../controller/change/change';

const handler = nc({
    onError: (err, req: NextApiRequest, res: NextApiResponse, next) => {
        console.error(err.stack);
        res.status(500).end("Something broke!");
    },
    onNoMatch: (req, res) => {
        res.status(404).end("Page is not found");
    },
}).use(cors())
    .get(getChange)
export default handler;