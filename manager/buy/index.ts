import { getConnection } from '../../database/db';
import { NextApiRequest, NextApiResponse } from 'next';
import { Change } from '../../src/entity/Change'
import { Items } from '../../src/entity/Items';
import { SaleOrder } from '../../src/entity/SaleOrder';

interface BalanceChange {
    amount: number;
    qty: number
}

export const buyItem = async (req: NextApiRequest, res: NextApiResponse) => {
    let balance_change = req.body.return_change
    let change: any
    balance_change.forEach(async (e: BalanceChange) => {
        const conn = await getConnection();

        let changeRepo = conn.getRepository(Change)
        change = await changeRepo.find({
            where: {
                amount: e.amount,
            },
        });

        let cal = change[0].qty - e.qty
        await conn
            .createQueryBuilder()
            .update(Change)
            .set({ qty: cal > 0 ? cal : 0 })
            .where("amount = :amount", { amount: e.amount })
            .execute();
        conn.close()
    });

    let item_id = req.body.item_id
    let item: any
    const conn = await getConnection();
    let itemsRepository = conn.getRepository(Items)
    item = await itemsRepository.find({
        where: {
            id: item_id,
        },
    });
    let _qty = item[0].qty - 1

    await conn
        .createQueryBuilder()
        .update(Items)
        .set({ qty: _qty > 0 ? _qty : 0 })
        .where("id = :id", { id: item_id })
        .execute();

    await conn
        .createQueryBuilder()
        .insert()
        .into(SaleOrder)
        .values({
            item_name: item[0].name,
            price: item[0].price
        })
        .execute()

    conn.close()

    res.statusCode = 200
    res.send(200)
}