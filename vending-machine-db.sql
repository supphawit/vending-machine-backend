/*
 Navicat Premium Data Transfer

 Source Server         : vending-machine
 Source Server Type    : PostgreSQL
 Source Server Version : 140002
 Source Host           : localhost:5432
 Source Catalog        : postgres
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140002
 File Encoding         : 65001

 Date: 18/03/2022 16:54:31
*/


-- ----------------------------
-- Sequence structure for change_copy1_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."change_copy1_id_seq";
CREATE SEQUENCE "public"."change_copy1_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
ALTER SEQUENCE "public"."change_copy1_id_seq" OWNER TO "root";

-- ----------------------------
-- Sequence structure for change_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."change_id_seq";
CREATE SEQUENCE "public"."change_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
ALTER SEQUENCE "public"."change_id_seq" OWNER TO "root";

-- ----------------------------
-- Sequence structure for items_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."items_id_seq";
CREATE SEQUENCE "public"."items_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
ALTER SEQUENCE "public"."items_id_seq" OWNER TO "root";

-- ----------------------------
-- Sequence structure for sale_order_copy1_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sale_order_copy1_id_seq";
CREATE SEQUENCE "public"."sale_order_copy1_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
ALTER SEQUENCE "public"."sale_order_copy1_id_seq" OWNER TO "root";

-- ----------------------------
-- Sequence structure for sale_order_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sale_order_id_seq";
CREATE SEQUENCE "public"."sale_order_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
ALTER SEQUENCE "public"."sale_order_id_seq" OWNER TO "root";

-- ----------------------------
-- Table structure for change
-- ----------------------------
DROP TABLE IF EXISTS "public"."change";
CREATE TABLE "public"."change" (
  "id" int4 NOT NULL DEFAULT nextval('change_id_seq'::regclass),
  "amount" int4 NOT NULL,
  "qty" int4 NOT NULL
)
;
ALTER TABLE "public"."change" OWNER TO "root";

-- ----------------------------
-- Records of change
-- ----------------------------
BEGIN;
INSERT INTO "public"."change" VALUES (7, 500, 13);
INSERT INTO "public"."change" VALUES (6, 100, 17);
INSERT INTO "public"."change" VALUES (5, 50, 47);
INSERT INTO "public"."change" VALUES (4, 20, 95);
INSERT INTO "public"."change" VALUES (3, 10, 104);
INSERT INTO "public"."change" VALUES (1, 1, 11);
INSERT INTO "public"."change" VALUES (2, 5, 9);
INSERT INTO "public"."change" VALUES (8, 1000, 3);
COMMIT;

-- ----------------------------
-- Table structure for change_copy1
-- ----------------------------
DROP TABLE IF EXISTS "public"."change_copy1";
CREATE TABLE "public"."change_copy1" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "amount" varchar(255) COLLATE "pg_catalog"."default",
  "qty" int4
)
;
ALTER TABLE "public"."change_copy1" OWNER TO "root";

-- ----------------------------
-- Records of change_copy1
-- ----------------------------
BEGIN;
INSERT INTO "public"."change_copy1" OVERRIDING SYSTEM VALUE VALUES (4, '20', 100);
INSERT INTO "public"."change_copy1" OVERRIDING SYSTEM VALUE VALUES (5, '50', 50);
INSERT INTO "public"."change_copy1" OVERRIDING SYSTEM VALUE VALUES (6, '100', 25);
INSERT INTO "public"."change_copy1" OVERRIDING SYSTEM VALUE VALUES (7, '500', 15);
INSERT INTO "public"."change_copy1" OVERRIDING SYSTEM VALUE VALUES (8, '1000', 5);
INSERT INTO "public"."change_copy1" OVERRIDING SYSTEM VALUE VALUES (1, '1', 500);
INSERT INTO "public"."change_copy1" OVERRIDING SYSTEM VALUE VALUES (2, '5', 300);
INSERT INTO "public"."change_copy1" OVERRIDING SYSTEM VALUE VALUES (3, '10', 200);
COMMIT;

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS "public"."items";
CREATE TABLE "public"."items" (
  "id" int4 NOT NULL DEFAULT nextval('items_id_seq'::regclass),
  "name" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "price" int4 NOT NULL,
  "qty" int4 NOT NULL,
  "img_size" int4 NOT NULL
)
;
ALTER TABLE "public"."items" OWNER TO "root";

-- ----------------------------
-- Records of items
-- ----------------------------
BEGIN;
INSERT INTO "public"."items" VALUES (1, 'sprite', 12, 12, 30);
INSERT INTO "public"."items" VALUES (6, 'kitkat', 25, 11, 50);
INSERT INTO "public"."items" VALUES (4, 'lays', 20, 3, 50);
INSERT INTO "public"."items" VALUES (3, 'fanta', 12, 5, 30);
INSERT INTO "public"."items" VALUES (8, 'green_tea', 15, 13, 60);
INSERT INTO "public"."items" VALUES (9, 'redbull', 15, 15, 80);
INSERT INTO "public"."items" VALUES (5, 'mentos', 10, 2, 100);
INSERT INTO "public"."items" VALUES (7, 'yoghurt', 16, 8, 60);
INSERT INTO "public"."items" VALUES (2, 'pepsi', 15, 14, 30);
COMMIT;

-- ----------------------------
-- Table structure for sale_order
-- ----------------------------
DROP TABLE IF EXISTS "public"."sale_order";
CREATE TABLE "public"."sale_order" (
  "id" int4 NOT NULL DEFAULT nextval('sale_order_id_seq'::regclass),
  "item_name" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "price" int4 NOT NULL,
  "create_at" timestamp(6) NOT NULL DEFAULT now()
)
;
ALTER TABLE "public"."sale_order" OWNER TO "root";

-- ----------------------------
-- Records of sale_order
-- ----------------------------
BEGIN;
INSERT INTO "public"."sale_order" VALUES (3, 'lays', 20, '2022-03-18 01:19:24.842805');
INSERT INTO "public"."sale_order" VALUES (4, 'mentos', 10, '2022-03-18 01:20:16.505839');
INSERT INTO "public"."sale_order" VALUES (5, 'mentos', 10, '2022-03-18 01:22:59.905982');
INSERT INTO "public"."sale_order" VALUES (6, 'mentos', 10, '2022-03-18 01:23:04.303586');
INSERT INTO "public"."sale_order" VALUES (7, 'mentos', 10, '2022-03-18 01:26:04.348898');
INSERT INTO "public"."sale_order" VALUES (8, 'lays', 20, '2022-03-18 01:27:16.921333');
INSERT INTO "public"."sale_order" VALUES (9, 'lays', 20, '2022-03-18 01:27:58.754776');
INSERT INTO "public"."sale_order" VALUES (10, 'lays', 20, '2022-03-18 01:32:50.905111');
INSERT INTO "public"."sale_order" VALUES (11, 'fanta', 12, '2022-03-18 01:37:44.314128');
INSERT INTO "public"."sale_order" VALUES (12, 'fanta', 12, '2022-03-18 01:38:34.575544');
INSERT INTO "public"."sale_order" VALUES (13, 'fanta', 12, '2022-03-18 01:39:48.143093');
INSERT INTO "public"."sale_order" VALUES (14, 'lays', 20, '2022-03-18 01:40:04.82709');
INSERT INTO "public"."sale_order" VALUES (15, 'fanta', 12, '2022-03-18 01:41:42.363916');
INSERT INTO "public"."sale_order" VALUES (16, 'mentos', 10, '2022-03-18 01:42:01.598666');
INSERT INTO "public"."sale_order" VALUES (17, 'redbull', 15, '2022-03-18 01:42:14.15567');
INSERT INTO "public"."sale_order" VALUES (18, 'redbull', 15, '2022-03-18 01:45:46.624867');
INSERT INTO "public"."sale_order" VALUES (19, 'redbull', 15, '2022-03-18 02:04:42.186557');
INSERT INTO "public"."sale_order" VALUES (20, 'fanta', 12, '2022-03-18 02:10:08.273062');
INSERT INTO "public"."sale_order" VALUES (21, 'mentos', 10, '2022-03-18 09:09:10.034925');
INSERT INTO "public"."sale_order" VALUES (22, 'green_tea', 15, '2022-03-18 09:10:33.672381');
INSERT INTO "public"."sale_order" VALUES (23, 'redbull', 15, '2022-03-18 09:10:58.144706');
INSERT INTO "public"."sale_order" VALUES (24, 'mentos', 10, '2022-03-18 09:11:12.77788');
INSERT INTO "public"."sale_order" VALUES (25, 'yoghurt', 16, '2022-03-18 09:42:41.366483');
INSERT INTO "public"."sale_order" VALUES (26, 'pepsi', 15, '2022-03-18 09:45:39.103481');
COMMIT;

-- ----------------------------
-- Table structure for sale_order_copy1
-- ----------------------------
DROP TABLE IF EXISTS "public"."sale_order_copy1";
CREATE TABLE "public"."sale_order_copy1" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "item_name" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "price" int4 NOT NULL,
  "create_at" timestamp(6) NOT NULL
)
;
ALTER TABLE "public"."sale_order_copy1" OWNER TO "root";

-- ----------------------------
-- Records of sale_order_copy1
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for typeorm_metadata
-- ----------------------------
DROP TABLE IF EXISTS "public"."typeorm_metadata";
CREATE TABLE "public"."typeorm_metadata" (
  "type" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "database" varchar COLLATE "pg_catalog"."default",
  "schema" varchar COLLATE "pg_catalog"."default",
  "table" varchar COLLATE "pg_catalog"."default",
  "name" varchar COLLATE "pg_catalog"."default",
  "value" text COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."typeorm_metadata" OWNER TO "root";

-- ----------------------------
-- Records of typeorm_metadata
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."change_copy1_id_seq"
OWNED BY "public"."change_copy1"."id";
SELECT setval('"public"."change_copy1_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."change_id_seq"
OWNED BY "public"."change"."id";
SELECT setval('"public"."change_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."items_id_seq"
OWNED BY "public"."items"."id";
SELECT setval('"public"."items_id_seq"', 3, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sale_order_copy1_id_seq"
OWNED BY "public"."sale_order_copy1"."id";
SELECT setval('"public"."sale_order_copy1_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sale_order_id_seq"
OWNED BY "public"."sale_order"."id";
SELECT setval('"public"."sale_order_id_seq"', 27, true);

-- ----------------------------
-- Primary Key structure for table change
-- ----------------------------
ALTER TABLE "public"."change" ADD CONSTRAINT "PK_38168d337b66a2d98e059fe5820" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table change_copy1
-- ----------------------------
ALTER TABLE "public"."change_copy1" ADD CONSTRAINT "change_copy1_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table items
-- ----------------------------
ALTER TABLE "public"."items" ADD CONSTRAINT "PK_ba5885359424c15ca6b9e79bcf6" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sale_order
-- ----------------------------
ALTER TABLE "public"."sale_order" ADD CONSTRAINT "PK_6f16972488e48db5935eae6011f" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sale_order_copy1
-- ----------------------------
ALTER TABLE "public"."sale_order_copy1" ADD CONSTRAINT "sale_order_copy1_pkey" PRIMARY KEY ("id");
